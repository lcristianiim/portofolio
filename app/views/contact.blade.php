@extends('layouts.master_page')

@section('body')

<header role="banner" id="top" class="navbar navbar-static-top bs-docs-nav">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../">Portofolio</a>
        </div>
        <nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
            <ul class="nav navbar-nav">
                <li id="about" class="active">
                    <a href="/">About</a>
                </li>
                <li id="contact">
                    <a href="contact">Contact</a>
                </li>



            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Projects <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://162.243.86.132:10002/">CitiesProjects</a></li>
                        <li><a href="http://162.243.86.132:10003/">PasswordManager</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">Work in progress</li>

                        <li><a href="http://162.243.86.132:10004/">autosattler (responsive)</a></li>
                        <li><a href="http://162.243.86.132:10005/">despremamesicopii.ro</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</header>

<div class="container marketing">
    <hr class="featurette-divider">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
            <!--            <img alt="140x140" data-src="holder.js/140x140" class="img-circle" style="width: 140px; height: 140px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+">-->
            <!--            <h2>Heading</h2>-->
            <!--            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>-->
            <!--            <p><a role="button" href="#" class="btn btn-default">View details »</a></p>-->
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
            <img alt="140x140"  class="img-circle" style="width: 140px; height: 140px;" src="images/pictures/PozaCV.jpg">
            <h2>Szabo Cristian-Marcel</h2>
            <p>Email: lcristianiim@yahoo.com</p>
            <p>Skype: cristianiim</p>
            <p>Phone: 0040 724 051 201</p>

            <div class="separator2"></div>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <!--            <img alt="140x140" data-src="holder.js/140x140" class="img-circle" style="width: 140px; height: 140px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+">-->
            <!--            <h2>Heading</h2>-->
            <!--            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>-->
            <!--            <p><a role="button" href="#" class="btn btn-default">View details »</a></p>-->
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
    <hr class="featurette-divider">

    <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 by Cristian.
    </footer>
</div>

@endsection



@section('scripts')
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/custom_scripts.js"></script>
<script type="text/javascript" src="bootstrap_add/js/docs.min.js"></script>
@endsection

