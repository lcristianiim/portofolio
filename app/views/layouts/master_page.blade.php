<!DOCTYPE html>
<html lang="en">
<head>
    <!--
    ****************************************************
    (c) Portofolio

    (c)2014

    Design & Development by Cristian

    ****************************************************
    -->

	<title>Portofolio</title>

    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">

    <meta name="author" content="Szabo Cristian-Marcel">
    <meta name="publisher" content="Szabo Cristian-Marcel">
    <meta name="page-topic" content="Portofolio">
    <meta name="page-type" content="Portofolio">

	<link rel="stylesheet" type="text/css" href="bootstrap-3.1.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap_add/css/carousel.css"/>
    <link rel="stylesheet" href="bootstrap_add/css/docs.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/custom_css.css">



	@yield('custom_head')
</head>

<body>
	@yield('body')
    @yield('scripts')
</body>
</html>