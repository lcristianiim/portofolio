@extends('layouts.master_page')

@section('body')
<header role="banner" id="top" class="navbar navbar-static-top bs-docs-nav">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".bs-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../">Portofolio</a>
        </div>
        <nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/">About</a>
                </li>
                <li>
                    <a href="contact">Contact</a>
                </li>



            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Projects <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://162.243.86.132:10002/">CitiesProjects</a></li>
                        <li><a href="http://162.243.86.132:10008/">DentistCabinet. For CMS: <strong>http://162.243.86.132:10008/administrare</strong>  User: lcristianiim@yahoo.com; Pass: aaa</a></li>
                        
                        
                        
                        <li class="divider"></li>
                        <li class="dropdown-header">Work in progress</li>

                        <li><a href="http://162.243.86.132:10004/">autosattler (responsive). For CMS: <strong>http://162.243.86.132:10004/login</strong>  User: lcristianiim@yahoo.com; Pass: aaa</a></li>
                        <li><a href="http://162.243.86.132:10003/">PasswordManager</a></li>
                        <li><a href="http://162.243.86.132:10006/">accountant</a></li>
                        <li><a href="http://162.243.86.132:10005/">despremamesicopii.ro</a></li>
                        <li><a href="http://178.62.40.240/despremamesicopii/">despremamesicopii.ro (second version)</a></li>
                        
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</header>


<div class="container marketing">
    <hr class="featurette-divider">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
<!--            <img alt="140x140" data-src="holder.js/140x140" class="img-circle" style="width: 140px; height: 140px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+">-->
<!--            <h2>Heading</h2>-->
<!--            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>-->
<!--            <p><a role="button" href="#" class="btn btn-default">View details »</a></p>-->
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
            <img alt="140x140"  class="img-circle" style="width: 140px; height: 140px;" src="images/pictures/PozaCV.jpg">
            <h2>Szabo Cristian-Marcel</h2>
            <p>Age: 28</p>
            <p>City: Oradea</p>
            <p>Coding skills:</p>
            <p>HTML/CSS</p>
            <p>PHP</p>
            <p>MySql</p>
            <p>jQuery/javascript</p>

            <div class="separator"></div>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
<!--            <img alt="140x140" data-src="holder.js/140x140" class="img-circle" style="width: 140px; height: 140px;" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+">-->
<!--            <h2>Heading</h2>-->
<!--            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>-->
<!--            <p><a role="button" href="#" class="btn btn-default">View details »</a></p>-->
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
    <hr class="featurette-divider">

    <!-- START THE FEATURETTES -->


    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Studing PHP! </h2>
            <p class="lead">Basically I have started to learn PHP, MySql, javascript/jQuery on my own with basic knowledge of coding from highscool and from personal study (HTML, Turbo Pascal, FoxPro, Macromedia Flash) and with the help of my brother who is a back end developer.</p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/php.jpg" class="featurette-image img-responsive" >
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img alt="500x500" class="featurette-image img-responsive" src="images/articles/laravel.jpg" >
        </div>
        <div class="col-md-7">
            <h2 class="featurette-heading"> Working with Laravel framework.<span class="text-muted"> A real help was Jeffrey Way from laracasts.com</span></h2>

            <p class="lead"> This site and all the projects showed here as portofolio are using the laravel framework. </p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Welcome to Twitter Bootstrap! <span class="text-muted">A great tool the spears you from a lot of pain in the css :)</span></h2>
            <p class="lead">This site and all the projects showed here as portofolio are using twitter bootstrap. </p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/bootstrap.jpg" class="featurette-image img-responsive" >
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img alt="500x500" class="featurette-image img-responsive" src="images/articles/vagrant.jpg" >
        </div>
        <div class="col-md-7">
            <h2 class="featurette-heading"> <span class="text-muted"> If Carlsberg is probably the best beer in the world,</span> Vagrant is probably the best local environment to develop locally.</h2>

            <p class="lead"> This site and all the projects showed here as portofolio were developed locally on a vagrant machine with oracle VM. www.vagrantbox.es is a help.
            </p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Great bridge between development and production. <span class="text-muted">Great for sharing and secondary backup.</span></h2>
            <p class="lead">This site and all the projects showed here as portofolio were developed locally, pushed at bitbucket.org, and pulled on a server from digital ocean. </p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/bitbucket.jpg" class="featurette-image img-responsive" >
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img alt="500x500" class="featurette-image img-responsive" src="images/articles/photoshop.jpg" >
        </div>
        <div class="col-md-7">
            <h2 class="featurette-heading"> One of the best software ever made. <span class="text-muted"> Discoverd in 2002.</span></h2>

            <p class="lead"> Back then it was Photoshop 7. When I first used the clone stamp tool on a picture with some trees, and "made" another tree apear in the picture, I was freaking WOOWWWW!!!! I have worked for 1 year at a Kodak photoshop in Oradea as a graphic designer and photographer.
                If you need something to be done in Photoshop, definitely Im youre guy. I've studied Illustrator, Lightroom also.
            </p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">My everlasting hobbies <span class="text-muted">Whenever there is time</span></h2>
            <p class="lead">I love messing around with these gourgeous software. Learn, learn, learn... never stop learning :) </p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/desktop.jpg" class="featurette-image img-responsive" >
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <iframe  width="500" height="500" src="//www.youtube.com/embed/f9EYF9u6BvI" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="col-md-7" style="padding-left: 50px">
            <h2 class="featurette-heading"> Testing my cool Rode Procaster mic and the Scarlet 2i2 recording interface. <span class="text-muted"> Screencast tutorial.</span></h2>
            <p class="lead"> A short tutorial on how to record youre own screen.
            </p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Attitude is everything! <span class="text-muted">Keep it simple.</span></h2>
            <p class="lead">Keep it simple and strait, as your code! :) </p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/attitude.jpg" class="featurette-image img-responsive" >
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img alt="500x500" class="featurette-image img-responsive" src="images/articles/ontheedge.jpg" >
        </div>
        <div class="col-md-7">
            <h2 class="featurette-heading"> Who is not worthy of small things cannot be worthy of big things.</h2>

            <p class="lead"> Be able to master small things to be able to master big things.
            </p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Keep youreself in shape! <span class="text-muted">Exercise.</span></h2>
            <p class="lead">
                For your code to be slim. </br>
                For your mind and body to be at full capacity.
            </p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/exercise.jpg" class="featurette-image img-responsive" >
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-5">
            <img alt="500x500" src="images/articles/comodore64.jpg" class="featurette-image img-responsive" >
        </div>
        <div class="col-md-7">
            <h2 class="featurette-heading">My first computer ever! <span class="text-muted">I still have this awsome babe!</span></h2>
            <p class="lead">I still remember the first command I have written back in 1994, when I was 8 years old. It was "load" :) .Back in thouse days there was no internet in Oradea and to start a game didn't meant a simple push of a button. Starting a game was a really awsome thing. The computer didn't had a monitor. The monitor was your TV!</p>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Upgrate to Pentium I at 166Mhz!  <span class="text-muted"></span></h2>
            <p class="lead">My first love.. Pentium I at 166Mhz, with a quantum BigFoot hard disk with 2,5 Gb and 16 edo ram. In 1997 this configuration was freaking amaizing! The hard disk is still functioning perfectly. These days they don't make things as in the past. The hard disk has a very specific running noise. When you pick it up in you're hand you have the feeling of quality. </p>
        </div>
        <div class="col-md-5">
            <img alt="500x500" class="featurette-image img-responsive" src="images/articles/harddisk.jpg" >
        </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->


    <!-- FOOTER -->
    <footer>
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 by Cristian.
    </footer>

</div>
@endsection

@section('scripts')
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/custom_scripts.js"></script>
<script type="text/javascript" src="bootstrap_add/js/docs.min.js"></script>
@endsection

